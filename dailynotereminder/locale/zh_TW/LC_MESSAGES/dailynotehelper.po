# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Genshin Dailynote Helper\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-09-07 13:24+0800\n"
"PO-Revision-Date: 2023-09-07 13:37+0800\n"
"Last-Translator: KT-Yeh\n"
"Language-Team: \n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.3.2\n"

#: dailynotereminder/check.py:28 dailynotereminder/getinfo/parse_info.py:120
msgid "奖励已领取"
msgstr "獎勵已領取"

#: dailynotereminder/check.py:31
msgid "你今日的委托还没有完成哦！"
msgstr "你今日的委託還沒有完成哦！"

#: dailynotereminder/check.py:32
msgid "🔔今日委托未完成，发送提醒。"
msgstr "🔔 今日委託未完成，發送提醒。"

#: dailynotereminder/check.py:34
msgid "你今日的委托奖励还没有领取哦！"
msgstr "你今日的委託獎勵還沒有領取哦！"

#: dailynotereminder/check.py:35
msgid "🔔今日委托已完成，奖励未领取，发送提醒。"
msgstr "🔔 今日委託已完成，獎勵未領取，發送提醒。"

#: dailynotereminder/check.py:37
msgid "✅委托检查结束，今日委托已完成，奖励已领取。"
msgstr "✅ 委託檢查結束，今日委託已完成，獎勵已領取。"

#: dailynotereminder/check.py:39
msgid "⏩︎未到每日委托检查提醒时间。"
msgstr "⏩︎未到每日委託檢查提醒時間。"

#: dailynotereminder/check.py:44
msgid "树脂已经溢出啦！"
msgstr "樹脂已經溢出啦！"

#: dailynotereminder/check.py:44
msgid "树脂快要溢出啦！"
msgstr "樹脂快要溢出啦！"

#: dailynotereminder/check.py:45
msgid "🔔树脂已到临界值，当前树脂{}，发送提醒。"
msgstr "🔔樹脂已到臨界值，當前樹脂{}，發送提醒。"

#: dailynotereminder/check.py:47
msgid "✅树脂检查结束，当前树脂{}，未到提醒临界值。"
msgstr "✅樹脂檢查結束，當前樹脂{}，未到提醒臨界值。"

#: dailynotereminder/check.py:56
msgid "洞天宝钱已经溢出啦！"
msgstr "洞天寶錢已經溢出啦！"

#: dailynotereminder/check.py:58
msgid "洞天宝钱快要溢出啦！"
msgstr "洞天寶錢快要溢出啦！"

#: dailynotereminder/check.py:61
msgid "🔔当前洞天宝钱{}，已到临界值{}，发送提醒。"
msgstr "🔔当前洞天寶錢{}，已到臨界值{}，發送提醒。"

#: dailynotereminder/check.py:67
msgid "✅洞天宝钱检查结束，未溢出。"
msgstr "✅洞天寶錢檢查結束，未溢出。"

#: dailynotereminder/check.py:74
msgid "✅探索派遣未全部完成。"
msgstr "✅探索派遣未全部完成。"

#: dailynotereminder/check.py:77
msgid "探索派遣已经完成啦！"
msgstr "探索派遣已經完成啦！"

#: dailynotereminder/check.py:78
msgid "🔔有已完成的探索派遣，发送提醒。"
msgstr "🔔有已完成的探索派遣，發送提醒。"

#: dailynotereminder/check.py:80
msgid "✅探索派遣检查结束，不存在完成的探索派遣。"
msgstr "✅探索派遣檢查結束，不存在完成的探索派遣。"

#: dailynotereminder/check.py:86
msgid "⏩︎未开启每日委托检查，已跳过。"
msgstr "⏩︎未開啟每日委託檢查，已跳過。"

#: dailynotereminder/check.py:91
msgid "⏩︎未开启树脂检查，已跳过。"
msgstr "⏩︎未開啟樹脂檢查，已跳過。"

#: dailynotereminder/check.py:96
msgid "⏩︎未开启洞天宝钱检查，已跳过。"
msgstr "⏩︎未開啟洞天寶錢檢查，已跳過。"

#: dailynotereminder/check.py:101
msgid "⏩︎未开启探索派遣完成提醒，已跳过。"
msgstr "⏩︎未開啟探索派遣完成提醒，已跳過。"

#: dailynotereminder/check.py:108
msgid "参量质变仪已就绪！"
msgstr "參數質變儀已就緒！"

#: dailynotereminder/check.py:109
msgid "🔔参量质变仪已就绪，发送提醒。"
msgstr "🔔參數質變儀已就緒，發送提醒。"

#: dailynotereminder/check.py:111
msgid "✅参量质变仪未准备好。"
msgstr "✅參數質變儀未準備好。"

#: dailynotereminder/check.py:113
msgid "⏩︎未获得参量质变仪。"
msgstr "⏩︎未獲得參數質變儀。"

#: dailynotereminder/check.py:115
msgid "⏩︎接口未返回参量质变仪信息。"
msgstr "⏩︎接口未返回參數質變儀信息。"

#: dailynotereminder/check.py:117
msgid "⏩︎未开启参量质变仪就绪提醒，已跳过。"
msgstr "⏩︎未開啟參數質變儀就緒提醒，已跳過。"

#: dailynotereminder/check.py:144
msgid "树脂将会在{}溢出，睡前记得清树脂哦！"
msgstr "樹脂將會在{}溢出，睡前記得清樹脂哦！"

#: dailynotereminder/check.py:145
msgid "🔔睡眠期间树脂将会溢出，发送提醒。"
msgstr "🔔睡眠期間樹脂將會溢出，發送提醒。"

#: dailynotereminder/check.py:148
msgid "✅睡眠期间树脂不会溢出，放心休息。"
msgstr "✅睡眠期間樹脂不會溢出，放心休息。"

#: dailynotereminder/check.py:154
msgid "⚠️处于轻量模式，仅检查树脂、委托、洞天宝钱。"
msgstr "⚠️處於輕量模式，僅檢查樹脂、委託、洞天寶錢。"

#: dailynotereminder/check.py:167 dailynotereminder/check.py:187
msgid "获取UID: {} 数据失败！"
msgstr "獲取UID: {} 資料失敗！"

#: dailynotereminder/check.py:181
msgid "⚠️UID: {} 账号异常，自动回落到轻量模式。"
msgstr "⚠️UID: {} 帳號異常，自動回落到輕量模式。"

#: dailynotereminder/check.py:195
msgid "🗝️ 当前配置了{}个账号，正在执行第{}个"
msgstr "🗝️ 目前配置了{}個帳號，正在執行第{}個"

#: dailynotereminder/check.py:205
#, python-brace-format
msgid "获取到{0}的{1}个角色..."
msgstr "獲取到{0}的{1}個角色..."

#: dailynotereminder/check.py:206
msgid "国服"
msgstr "國服"

#: dailynotereminder/check.py:206
msgid "国际服"
msgstr "國際服"

#: dailynotereminder/check.py:211
msgid "第{}个角色，{} {}"
msgstr "第{}個角色，{} {}"

#: dailynotereminder/check.py:216
msgid "跳过该角色"
msgstr "跳過該角色"

#: dailynotereminder/check.py:226
msgid "获取米游社角色信息失败！"
msgstr "獲取遊戲角色資料失敗！"

#: dailynotereminder/__init__.py:20
msgid "😴休眠中……"
msgstr "😴休眠中……"

#: dailynotereminder/__init__.py:28
msgid "本轮运行结束，等待下次检查..."
msgstr "本輪執行結束，等待下次檢查..."

#: dailynotereminder/utils/update_checker.py:63
msgid ""
"当前版本：{}\n"
"最新版本：{}\n"
"更新地址：{}"
msgstr ""
"當前版本：{}\n"
"最新版本：{}\n"
"更新地址：{}"

#: dailynotereminder/utils/update_checker.py:69
msgid "⬆️ 检查到新版本{}，请及时更新。"
msgstr "⬆️ 檢查到新版本{}，請及時更新。"

#: dailynotereminder/utils/update_checker.py:71
msgid "Genshin-Dailynote-Reminder 有更新啦"
msgstr "Genshin-Dailynote-Reminder 有更新啦"

#: dailynotereminder/utils/update_checker.py:79
msgid "⚠️ 检查版本更新失败。"
msgstr "⚠️ 檢查版本更新失敗。"

#: dailynotereminder/utils/update_checker.py:85
msgid "🔄 当前已是最新版本，无需更新。"
msgstr "🔄 當前已是最新版本，無需更新。"

#: dailynotereminder/getinfo/client_cn_widget.py:59
#: dailynotereminder/getinfo/client.py:81
msgid "获取数据失败！"
msgstr "獲取資料失敗！"

#: dailynotereminder/getinfo/client_cn_widget.py:112
msgid "Cookie 中缺少 login_ticket，请重新获取完整 Cookie！"
msgstr "Cookie 中缺少 login_ticket，請重新獲取完整 Cookie！"

#: dailynotereminder/getinfo/client.py:44
msgid "正在获取角色信息"
msgstr "正在獲取角色資料"

#: dailynotereminder/getinfo/client.py:93
msgid "未开启实时便笺！"
msgstr "未開啟即時便箋！"

#: dailynotereminder/getinfo/client.py:95
msgid "账号异常！请登录米游社APP进行验证。"
msgstr "您的賬戶異常，請在 Hoyolab 應用程序中進行驗證。"

#: dailynotereminder/getinfo/parse_info.py:13
msgid "天空岛 🌈"
msgstr "天空島 🌈"

#: dailynotereminder/getinfo/parse_info.py:14
msgid "世界树 🌲"
msgstr "世界樹 🌲"

#: dailynotereminder/getinfo/parse_info.py:15
msgid "美服 🦙"
msgstr "美服 🦙"

#: dailynotereminder/getinfo/parse_info.py:16
msgid "欧服 🏰"
msgstr "歐服 🏰"

#: dailynotereminder/getinfo/parse_info.py:17
msgid "亚服 🐯"
msgstr "亞服 🐯"

#: dailynotereminder/getinfo/parse_info.py:18
msgid "台港澳服 🧋"
msgstr "臺港澳服 🧋"

#: dailynotereminder/getinfo/parse_info.py:80
#: dailynotereminder/getinfo/parse_info.py:138
msgid "当前洞天宝钱/上限：{} / {}\n"
msgstr "當前洞天寶錢/上限：{} / {}\n"

#: dailynotereminder/getinfo/parse_info.py:92
msgid "当前树脂：{} / {}\n"
msgstr "當前樹脂：{} / {}\n"

#: dailynotereminder/getinfo/parse_info.py:98
msgid "下个回复倒计时：{}\n"
msgstr "下個恢復倒數計時：{}\n"

#: dailynotereminder/getinfo/parse_info.py:106
#: dailynotereminder/getinfo/parse_info.py:166
msgid "今天"
msgstr "今天"

#: dailynotereminder/getinfo/parse_info.py:106
#: dailynotereminder/getinfo/parse_info.py:166
msgid "明天"
msgstr "明天"

#: dailynotereminder/getinfo/parse_info.py:107
msgid "预估回复时间：{} {}"
msgstr "預估恢復時間：{} {}"

#: dailynotereminder/getinfo/parse_info.py:112
msgid "周本树脂减半：{} / {}"
msgstr "週本樹脂減半：{} / {}"

#: dailynotereminder/getinfo/parse_info.py:122
msgid "奖励未领取"
msgstr "獎勵未領取"

#: dailynotereminder/getinfo/parse_info.py:124
msgid "委托未完成"
msgstr "委託未完成"

#: dailynotereminder/getinfo/parse_info.py:125
msgid ""
"今日委托任务：{}   {}\n"
"--------------------"
msgstr ""
"今日委託任務：{}   {}\n"
"--------------------"

#: dailynotereminder/getinfo/parse_info.py:130
msgid "周一"
msgstr "週一"

#: dailynotereminder/getinfo/parse_info.py:131
msgid "周二"
msgstr "週二"

#: dailynotereminder/getinfo/parse_info.py:132
msgid "周三"
msgstr "週三"

#: dailynotereminder/getinfo/parse_info.py:133
msgid "周四"
msgstr "週四"

#: dailynotereminder/getinfo/parse_info.py:134
msgid "周五"
msgstr "週五"

#: dailynotereminder/getinfo/parse_info.py:135
msgid "周六"
msgstr "週六"

#: dailynotereminder/getinfo/parse_info.py:136
msgid "周日"
msgstr "週日"

#: dailynotereminder/getinfo/parse_info.py:145
msgid "洞天宝钱全部恢复时间：{} {}\n"
msgstr "洞天寶錢全部恢復時間：{} {}\n"

#: dailynotereminder/getinfo/parse_info.py:158
msgid "角色 {}"
msgstr "角色 {}"

#: dailynotereminder/getinfo/parse_info.py:160
msgid "  · {}：已完成"
msgstr "  · {}：已完成"

#: dailynotereminder/getinfo/parse_info.py:168
msgid "  · {} 完成时间：{}{}"
msgstr "  · {} 完成時間：{}{}"

#: dailynotereminder/getinfo/parse_info.py:178
msgid ""
"当前探索派遣总数/完成/上限：{}\n"
"{}"
msgstr ""
"當前探索派遣總數/完成/上限：{}\n"
"{}"

#: dailynotereminder/getinfo/parse_info.py:184
msgid "参量质变仪：已就绪"
msgstr "參數質變儀：已就緒"

#: dailynotereminder/getinfo/parse_info.py:186
msgid "参量质变仪： {} 天 {} 小时后可用"
msgstr "參數質變儀： {} 天 {} 小時後可用"

#: dailynotereminder/getinfo/parse_info.py:191
msgid "参量质变仪： 未获得"
msgstr "參數質變儀：未獲得"

#~ msgid "请查阅运行日志获取详细原因。"
#~ msgstr "請查閱執行紀錄獲取詳細原因。"

#~ msgid "亲爱的旅行者，"
#~ msgstr "親愛的旅行者，"

#~ msgid "已完成"
#~ msgstr "已完成"

#~ msgid "剩余时间"
#~ msgstr "剩餘時間"

from dailynotereminder.__banner__ import banner
from dailynotereminder import run_once

print(banner)
run_once()
